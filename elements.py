#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import smil


def main(filename_):

    filename = str(filename_)
    arc = smil.SMIL(filename)
    for element in arc.elements():
        print(element.name())


if __name__ == '__main__':
    main(sys.argv[1])
