#!/usr/bin/python3
# -*- coding: utf-8 -*-

import xml.dom.minidom
import xml.etree.ElementTree


class Element:
    def __init__(self, layout, atrs):
        self.element_name = layout
        self.atr_dic = atrs

    def name(self):
        return self.element_name

    def attrs(self):
        return self.atr_dic


class SMIL:
    def __init__(self, path):
        self.path = path

    def elements(self):
        documento = xml.etree.ElementTree.parse(self.path)
        elements = list()
        elementTypeList = list()

        for el in documento.iter():
            elements.append(el)

        for element in elements:
            elementTypeList.append(Element(element.tag, element.attrib))

        return elementTypeList


if __name__ == '__main__':
    obj = SMIL('karaoke.smil')
    x = obj.elements()
