#!/usr/bin/python3
# -*- coding: utf-8 -*-

import xml.dom.minidom


def main(path):
    document = xml.dom.minidom.parse(path)
    jokes = document.getElementsByTagName('chiste')

    buenisimo = []
    bueno = []
    regular = []
    malo = []
    malisimo = []

    for joke in jokes:
        score = joke.getAttribute('calificacion')

        questions = joke.getElementsByTagName('pregunta')
        question = questions[0].firstChild.nodeValue.strip()

        answers = joke.getElementsByTagName('respuesta')
        answer = answers[0].firstChild.nodeValue.strip()

        if score == "buenisimo":
            buenisimo.append(score)
            buenisimo.append(question)
            buenisimo.append(answer)
        elif score == "bueno":
            bueno.append(score)
            bueno.append(question)
            bueno.append(answer)
        elif score == "regular":
            regular.append(score)
            regular.append(question)
            regular.append(answer)
        elif score == "malo":
            malo.append(score)
            malo.append(question)
            malo.append(answer)
        elif score == "malisimo":
            malisimo.append(score)
            malisimo.append(question)
            malisimo.append(answer)

    if buenisimo:
        print("Calificación: " + buenisimo[0])
        print("- Pregunta: " + buenisimo[1])
        print("- Respuesta: " + buenisimo[2] + "\n")
    else:
        print("No hay chistes buenisimos")
    if bueno:
        print("Calificación: " + bueno[0])
        print("- Pregunta: " + bueno[1])
        print("- Respuesta: " + bueno[2] + "\n")
    else:
        print("No hay chistes buenos")
    if regular:
        print("Calificación: " + regular[0])
        print("- Pregunta: " + regular[1])
        print("- Respuesta: " + regular[2] + "\n")
    else:
        print("No hay chistes regulares")
    if malo:
        print("Calificación: " + malo[0])
        print("- Pregunta: " + malo[1])
        print("- Respuesta: " + malo[2] + "\n")
    else:
        print("No hay chistes malos")
    if malisimo:
        print("Calificación: " + malisimo[0])
        print("- Pregunta: " + malisimo[1])
        print("- Respuesta: " + malisimo[2] + "\n")
    else:
        print("No hay chistes malisimos")


if __name__ == "__main__":
    main("chistes8.xml")
