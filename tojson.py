#!/usr/bin/python3
# -*- coding: utf-8 -*-

import json
import smil


class SMILJSON(smil.SMIL):
    def json(self):
        d = []
        smil_file = smil.SMIL(self.path)

        for element in smil_file.elements():
            element_obj = ElementJSON(element.name(), element.attrs())
            d.append(element_obj.dic())

        json_str = json.dumps(d, indent=1)
        return json_str


class ElementJSON(smil.Element):
    def dic(self):
        json_dic = {'attrs': self.atr_dic, 'name': self.name()}
        return json_dic


def main(path):
    r = SMILJSON(path)
    r.json()
    print(r.json())


if __name__ == '__main__':
    main('karaoke.smil')
